using System;
using Random = Unity.Mathematics.Random;

namespace ZPowder.Threading
{
    public static class ThreadSafeRandom
    {
        private static Random random = new Random((uint)RandomSeed.Seed);

        public static float Value => random.NextFloat();
    }
}