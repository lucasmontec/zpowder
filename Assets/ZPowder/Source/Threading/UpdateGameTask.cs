using System.Collections.Generic;
using ZPowder.Util;

namespace ZPowder.Threading
{
    public class UpdateGameTask
    {
        private readonly DustGame dustGame;
        private readonly int jobPixels;

        private readonly List<int> pixelIDs = new List<int>();
        
        public float DeltaTime { private get; set; }

        public UpdateGameTask(DustGame dustGame, int startPixel, int jobPixels)
        {
            this.dustGame = dustGame;
            this.jobPixels = jobPixels;
            
            for (int pixelID = startPixel; pixelID < startPixel+jobPixels; pixelID++)
            {
                pixelIDs.Add(pixelID);
            }
            pixelIDs.Shuffle(10);
        }

        public void Execute()
        {
            //Stopwatch sw = Stopwatch.StartNew();
            for (int shuffleListID = 0; shuffleListID < jobPixels; shuffleListID++)
            {
                int pixelID = pixelIDs[shuffleListID];
                int x = pixelID % dustGame.Width;
                int y = pixelID / dustGame.Width;
                dustGame.UpdatePixel(DeltaTime, x, y);
            }
            //Debug.Log(sw.ElapsedMilliseconds);
        }
    }
}