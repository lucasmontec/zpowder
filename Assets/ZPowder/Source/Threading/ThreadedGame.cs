using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;
using ZPowder.Particle;
using ZPowder.Reactions;
using ZPowder.Rendering;
using ZPowder.Updaters;
using ZPowder.Updaters.Base;
using ZPowder.Util;

namespace ZPowder.Threading
{
    public class ThreadedGame : SerializedMonoBehaviour
    {
        public BufferScreenRenderer bufferScreenRenderer;
        public float resolutionMult = 0.5f;

        private ParticleBufferRenderer particleRenderer;

        private DustGame dustGame;

        public bool autoUpdate = true;

        [FormerlySerializedAs("particleBundle")] [FormerlySerializedAs("ParticleBundle")] 
        public ParticleUpdaterBundle particleUpdaterBundle;

        private Func<Particle.Particle> brush;

        [SerializeField]
        private int brushSize = 3;

        private RegionUpdateTask[,] gameUpdateJobs;
        private  readonly List<Task> runningJobsList = new List<Task>();
        
        [SerializeField, OnValueChanged(nameof(UpdateBottomToTop))]
        private bool updateFromBottomToTop;

        private ReactionGrid reactionGrid;
        public int reactionGridRatio = 8;
        
        private void UpdateBottomToTop(bool val)
        {
            if (Application.isPlaying)
            {
                dustGame.UpdateBottomToTop = val;
            }
        }
        
        private void Start()
        {
            brush = () => ParticleFactory.Sand;
            
            //TODO: MOVE w and h outside and unbounded by screen. Init particle renderer with dust game on constructor and dust game with fixed w and h.
            particleRenderer = new ParticleBufferRenderer(bufferScreenRenderer, resolutionMult);
            dustGame = new DustGame(particleRenderer.Width, particleRenderer.Height)
            {
                UpdateBottomToTop = updateFromBottomToTop
            };
            particleRenderer.SetDustGame(dustGame);
            
            reactionGrid = new ReactionGrid(dustGame, reactionGridRatio);
            
            RegisterParticleBundle();

            gameUpdateJobs = new RegionUpdateTask[dustGame.RegionColumns(), dustGame.RegionRows()];
            
            for (int rY = 0; rY < dustGame.RegionRows(); rY++)
            {
                for (int rX = 0; rX < dustGame.RegionColumns(); rX++)
                {
                    gameUpdateJobs[rX, rY] = new RegionUpdateTask(dustGame, rX, rY, 5);
                }
            }
        }

        private void RegisterParticleBundle()
        {
            foreach ((ParticleType type, ParticleDefinition definition) in particleUpdaterBundle.ParticleDefinitions)
            {
                AbstractParticleUpdater abstractParticleUpdater = definition.ParticleUpdater;
                
                dustGame.RegisterUpdater(type, abstractParticleUpdater.UpdateParticle);
                dustGame.RegisterDirtyChecker(type, abstractParticleUpdater.CheckDirty);
            }

            foreach (AbstractGenericParticleUpdater genericParticleUpdater in particleUpdaterBundle.GenericParticleUpdaters)
            {
                dustGame.RegisterGenericUpdater(genericParticleUpdater.UpdateParticle);

                if (genericParticleUpdater is IReactionGridListener gridListener)
                {
                    gridListener.SetReactionGrid(reactionGrid);
                }
            }
        }

        private void OnDestroy()
        {
            particleRenderer.Dispose();
        }

        private float MouseX => Mathf.Clamp(Input.mousePosition.x * resolutionMult, 0f, dustGame.Width-1f);
        private float MouseY => Mathf.Clamp(Input.mousePosition.y * resolutionMult, 0f, dustGame.Height-1f);
        
        private float lastMouseX, lastMouseY;
        private Vector2 mouseMovement;
        private Vector2 lastMouseDirection;

        [Button, HideIf(nameof(autoUpdate)), HideInEditorMode]
        public void Step()
        {
            dustGame.Update(Time.deltaTime);
            particleRenderer.Update();
        }

        private void Update()
        {
            MouseInput();

            if (Input.GetKeyDown(KeyCode.S))
            {
                brush = () => ParticleFactory.Sand;
            }
            
            if (Input.GetKeyDown(KeyCode.W))
            {
                brush = () => ParticleFactory.Water;
            }
            
            if (Input.GetKeyDown(KeyCode.R))
            {
                brush = () => ParticleFactory.Rock;
            }

            if (Input.GetKeyDown(KeyCode.O))
            {
                brush = () => ParticleFactory.Oil;
            }
            
            if (Input.GetKeyDown(KeyCode.G))
            {
                brush = () => ParticleFactory.Gasoline;
            }
            
            if (Input.GetKeyDown(KeyCode.P))
            {
                brush = () => ParticleFactory.GunPowder;
            }
            
            if (Input.GetKeyDown(KeyCode.T))
            {
                brush = ParticleFactory.Steam;
            }
            
            if (Input.GetKeyDown(KeyCode.F))
            {
                brush = ParticleFactory.Fire;
            }
            
            if (Input.GetKeyDown(KeyCode.L))
            {
                brush = () => ParticleFactory.Laser(-lastMouseDirection * 500f);
            }
            
            if (Input.GetKeyDown(KeyCode.E))
            {
                brush = ParticleFactory.Energy;
            }
            
            if (Input.GetKeyDown(KeyCode.D))
            {
                brush = () => ParticleFactory.Copper;
            }
            
            if (Input.GetKeyDown(KeyCode.C))
            {
                dustGame.ClearBoard();
            }
            
            if (Input.GetKeyDown(KeyCode.K))
            {
                dustGame.FillBoard(ParticleFactory.Water);
            }

            if (Input.GetKeyDown(KeyCode.A))
            {
                brush = () => ParticleFactory.Automata;
            }

            if (autoUpdate)
            {
                reactionGrid.Process(dustGame);
                
                dustGame.BeginRegionUpdate();
                
                UpdateRegionRound(false, false);
                UpdateRegionRound(true, false);
                UpdateRegionRound(false, true);
                UpdateRegionRound(true, true);

                dustGame.EndUpdate();
            }
            
            particleRenderer.Update();
        }

        private void UpdateRegionRound(bool oddX, bool oddY)
        {
            float deltaTime = Time.deltaTime;

            int startX = oddX ? 1 : 0;
            int startY = oddY ? 1 : 0;

            for (int rY = startY; rY < dustGame.RegionRows(); rY += 2)
            {
                for (int rX = startX; rX < dustGame.RegionColumns(); rX += 2)
                {
                    gameUpdateJobs[rX, rY].SetDeltaTime(deltaTime);
                    runningJobsList.Add(Task.Factory.StartNew(gameUpdateJobs[rX, rY].Update));
                }
            }

            Task.WhenAll(runningJobsList).Wait();
            runningJobsList.Clear();
        }

        private void MouseInput()
        {
            if (Input.GetMouseButton(0))
            {
                PaintBrush(brush);
            }

            if (Input.GetMouseButton(1))
            {
                PaintBrush(() => ParticleFactory.Air, true);
            }

            if (Input.GetMouseButtonDown(2))
            {
                PrintParticleInfoDebug();
            }
        }

        private void PrintParticleInfoDebug()
        {
            Particle.Particle p = dustGame[(int)MouseX, (int)MouseY];
            Debug.Log($"[{(int)MouseX}, {(int) MouseY}] {p.Type} : last tick({p.LastUpdateTick}) state({p.State}) lifetime({p.LifeTime}) - gameTick({dustGame.Tick})");
        }
        
        private void LateUpdate()
        {
            mouseMovement = new Vector2(lastMouseX - MouseX, lastMouseY - MouseY);
            if (mouseMovement.sqrMagnitude > 0)
            {
                lastMouseDirection = mouseMovement.normalized;
            }
            lastMouseX = MouseX;
            lastMouseY = MouseY;
        }

        private void PaintBrush(Func<Particle.Particle> particleProvider, bool overrideParticle=false)
        {
            int x0 = (int)lastMouseX;
            int x1 = (int)MouseX;
            int y0 = (int)lastMouseY;
            int y1 = (int)MouseY;
            
            //Bresenham
            int dx = Mathf.Abs(x1 - x0), sx = x0 < x1 ? 1 : -1;
            int dy = Mathf.Abs(y1 - y0), sy = y0 < y1 ? 1 : -1;
            int err = (dx > dy ? dx : -dy) / 2;
            for(;;) {
                PaintAt(particleProvider, overrideParticle, x0, y0);
                if (x0 == x1 && y0 == y1) break;
                int e2 = err;
                if (e2 > -dx) { err -= dy; x0 += sx; }
                if (e2 < dy) { err += dx; y0 += sy; }
            }
        }

        private void PaintAt(Func<Particle.Particle> particleProvider, bool overrideParticle, int x, int y)
        {
            for (int brushY = y - brushSize; brushY < y + brushSize; brushY++)
            {
                for (int brushX = x - brushSize; brushX < x + brushSize; brushX++)
                {
                    if (brushX < 0 || brushX > dustGame.Width - 1)
                    {
                        continue;
                    }

                    if (brushY < 0 || brushY > dustGame.Height - 1)
                    {
                        continue;
                    }

                    var distance = IntMath.Distance(brushX, brushY, x, y);
                    if (distance < brushSize)
                    {
                        if (overrideParticle || dustGame[brushX, brushY].IsAir())
                            dustGame.AddParticle(brushX, brushY, particleProvider());
                    }
                }
            }
        }
    }
}