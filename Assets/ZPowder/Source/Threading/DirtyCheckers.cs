using UnityEngine;
using ZPowder.Particle;
using ZPowder.Updaters;

namespace ZPowder.Threading
{
    public static class DirtyCheckers
    {
        public static bool VelocityDirectionSpaceDirty(int px, int py, float dt, ref ParticleBoard.ParticleBoard board)
        {
            Particle.Particle particle = board[px, py];
            if (board[px, py].HasLifetime) return true;

            int endX = Mathf.Clamp((int)(px + particle.Velocity.x * dt), 0, board.Width-1);
            int endY = Mathf.Clamp((int)(py + particle.Velocity.y * dt), 0, board.Height-1);

            int deltaX = endX - px;
            int deltaY = endY - py;

            if (deltaX == 0 && deltaY == 0)
            {
                return false;
            }
            
            int dirX = deltaX > 0 ? 1 : -1;
            int dirY = deltaY > 0 ? 1 : -1;
            
            for (int x = 0; x <= Mathf.Abs(deltaX); x++)
            {
                int realX = px + x*dirX;
                for (int y = 0; y <= Mathf.Abs(deltaY); y++)
                {
                    int realY = py + y*dirY;
                    
                    if(realX == px && realY == py)
                    {
                        continue;
                    }
                    
                    if (ParticleUpdaterUtils.CanDestroyAtSpace(px, py, realX, realY, ref board))
                    {
                        return true;
                    }

                    return false;
                }
            }

            return false;
        }
        
        public static bool MidTopSpaceDirty(int px, int py, float dt, ref ParticleBoard.ParticleBoard board)
        {
            if (board[px, py].HasLifetime) return true;
            
            int startX = Mathf.Max(0 , px - 1);
            int startY = py;

            int endX = Mathf.Min(board.Width-1, px + 1);
            int endY = Mathf.Min(board.Height-1, py + 1);

            for (int x = startX; x <= endX; x++)
            {
                for (int y = startY; y <= endY; y++)
                {
                    if (ParticleUpdaterUtils.CanOccupySpace(px, py, x, y, ref board))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
        
        public static bool MidBottomSpaceDirty(int px, int py, float dt, ref ParticleBoard.ParticleBoard board)
        {
            if (board[px, py].HasLifetime) return true;
            
            int startX = Mathf.Max(0 , px - 1);
            int startY = py;

            int endX = Mathf.Min(board.Width-1, px + 1);
            int endY = Mathf.Max(0, py - 1);

            for (int x = startX; x <= endX; x++)
            {
                for (int y = startY; y >= endY; y--)
                {
                    if (ParticleUpdaterUtils.CanOccupySpace(px, py, x, y, ref board))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
        
        public static bool BottomSpaceDirty(int px, int py, float dt, ref ParticleBoard.ParticleBoard board)
        {
            if (board[px, py].HasLifetime) return true;
            
            int startX = Mathf.Max(0 , px - 1);

            int endX = Mathf.Min(board.Width-1, px + 1);

            if (py - 1 < 0) return false;
            
            for (int x = startX; x <= endX; x++)
            {
                if (ParticleUpdaterUtils.CanOccupySpace(px, py, x, py - 1, ref board))
                {
                    return true;
                }
            }

            return false;
        }
    }
}