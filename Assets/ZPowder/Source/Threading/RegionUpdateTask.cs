namespace ZPowder.Threading
{
    public class RegionUpdateTask
    {
        private readonly DustGame dustGame;
        private readonly int regionX;
        private readonly int regionY;
        private readonly int updateSwitchSideTicks;

        private float dt;
        private int ticksToSwitchSide;

        private bool leftToRight;
        private bool bottomToTop = true;
        
        public RegionUpdateTask(DustGame dustGame, int regionX, int regionY, int updateSwitchSideTicks)
        {
            this.dustGame = dustGame;
            this.regionX = regionX;
            this.regionY = regionY;
            this.updateSwitchSideTicks = updateSwitchSideTicks;
            ticksToSwitchSide = updateSwitchSideTicks;
        }

        public void SetDeltaTime(float delta)
        {
            dt = delta;
        }
        
        public void Update()
        {
            if (ticksToSwitchSide-- <= 0)
            {
                ticksToSwitchSide = updateSwitchSideTicks;
                leftToRight = !leftToRight;
            }
            
            dustGame.UpdateRegion(leftToRight, bottomToTop, dt, regionX, regionY);
        }
    }
}