# Tasks

## Issues
Fire update is not working across cells.
IsDamaged is unused.
Everything from particles is defined separately. Creating new particles is not straight forward.

## General performance

### ThreadedGame.cs
This method: `runningJobsList.Add(Task.Factory.StartNew(gameUpdateJobs[rX, rY].Update));` generates garbage per frame.
A better way would be to create the tasks at the beginning instead of each frame, and to have tasks that can run indefinitely.

### Cellular Automatas

CellularAutomataUpdater dirty causes everything to update always. The dirty checker has no way currently of detecting stable configurations.