using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using UnityEngine.Serialization;
using ZPowder.Particle;

namespace ZPowder.Rendering
{
    [Serializable]
    [HideReferenceObjectPicker]
    internal struct HDRColor
    {
        [HideLabel]
        [FormerlySerializedAs("Color")]
        [ColorUsage(false, true)]
        public Color color;
    }
    
    [CreateAssetMenu]
    public class ParticleColorScheme : SerializedScriptableObject
    {
        [SerializeField][FormerlySerializedAs("particleColors")]
        private readonly Dictionary<ParticleType, HDRColor> particlePrimaryColors = new Dictionary<ParticleType, HDRColor>();
        
        [SerializeField][FormerlySerializedAs("particleLifetimeDecayColors")]
        private readonly Dictionary<ParticleType, HDRColor> particleSecondaryColors = new Dictionary<ParticleType, HDRColor>();

        public Color[] GetParticlePrimaryColorArray => GetColorArrayFromColorDictionary(particlePrimaryColors);
        
        public Color[] GetLifetimeDecayColorArray => GetColorArrayFromColorDictionary(particleSecondaryColors);

        private static Color[] GetColorArrayFromColorDictionary(IReadOnlyDictionary<ParticleType, HDRColor> colorDictionary)
        {
            var particleColorArray = new Color[256];
            for (int i = 0; i < 256; i++)
            {
                if (colorDictionary.ContainsKey((ParticleType) i))
                {
                    particleColorArray[i] = colorDictionary[(ParticleType) i].color;
                }
                else
                {
                    particleColorArray[i] = new Color(0,0,0,1); 
                }
            }

            return particleColorArray;
        }
    }
}