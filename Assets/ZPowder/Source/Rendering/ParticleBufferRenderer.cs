using System;
using UnityEngine;
using ZPowder.Particle;

namespace ZPowder.Rendering
{
    public class ParticleBufferRenderer : IDisposable
    {
        public int Height { get; }
        public int Width { get; }

        private readonly ParticleRenderData[] particles;

        private readonly BufferScreenRenderer renderer;
        private DustGame dustGame;

        private readonly ComputeBuffer pixelData;

        private struct ParticleRenderData
        {
            public int ParticleInfo;//32 bit
            public float Lifetime;//32 bit

            public void SetupParticleInfo(ParticleType type, bool isLiquid, bool hasLifetime, bool isCharged)
            {
                ParticleInfo = (byte)type;
                ParticleInfo |= (isLiquid ? 1 : 0) << 8;
                ParticleInfo |= (hasLifetime ? 1 : 0) << 9;
                ParticleInfo |= (isCharged ? 1 : 0) << 10;
            }
        }
        
        public ParticleBufferRenderer(BufferScreenRenderer renderer, float resolutionMult = 0.5f)
        {
            this.renderer = renderer;
            
            Width = (int) (Screen.width * resolutionMult);
            Height = (int) (Screen.height * resolutionMult);

            particles = new ParticleRenderData[Width * Height];
            InitParticlesRenderData();

            pixelData = new ComputeBuffer(Width * Height, sizeof(int) + sizeof(float));
            renderer.Initialize(Width, Height);
        }

        public void SetDustGame(DustGame game)
        {
            dustGame = game;
        }
        
        private void InitParticlesRenderData()
        {
            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    var index = Index(x, y);
                    particles[index] = new ParticleRenderData();
                }
            }
        }

        public void Update()
        {
            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    var index = Index(x, y);
                    Particle.Particle p = dustGame[x, y];
                    particles[index].Lifetime = p.NormalizedLifetime;
                    particles[index].SetupParticleInfo(p.Type, p.IsLiquid(), p.HasLifetime, p.IsCharged());
                }
            }
            pixelData.SetData(particles);
            renderer.SetBuffer(pixelData);
        }

        private int Index(int x, int y)
        {
            return y * Width + x;
        }

        public void Dispose()
        {
            pixelData?.Dispose();
        }
    }
}