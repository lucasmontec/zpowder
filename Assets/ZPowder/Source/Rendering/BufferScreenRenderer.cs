using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Rendering;
using ZPowder.Particle;

namespace ZPowder.Rendering
{
    public class BufferScreenRenderer : SerializedMonoBehaviour
    {
        [SerializeField] private ParticleColorScheme particleColorScheme;
        [SerializeField] private Camera renderCamera;

        private CommandBuffer commandBuffer;
        
        public Shader shader;
        private Material renderMaterial;

        private static readonly int ParticleLifetimeColors = Shader.PropertyToID("_ParticleLifetimeColors");
        private static readonly int Colors = Shader.PropertyToID("_ParticleColors");
        
        private static readonly int Particles = Shader.PropertyToID("_Particles");
        private static readonly int Width = Shader.PropertyToID("_Width");
        private static readonly int Height = Shader.PropertyToID("_Height");
        
        private bool initialized;

        public void Initialize(int width, int height)
        {
            if(initialized) return;
            
            if (shader == null)
            {
                Debug.LogError("no shader.");
                renderMaterial = null;
                return;
            }
            renderMaterial = new Material(shader);
            initialized = true;
            renderMaterial.SetInt(Width, width);
            renderMaterial.SetInt(Height, height);
            
            renderMaterial.SetColorArray(Colors, particleColorScheme.GetParticlePrimaryColorArray);
            renderMaterial.SetColorArray(ParticleLifetimeColors, particleColorScheme.GetLifetimeDecayColorArray);

            SetupCommandBuffer();
        }

        private void OnDestroy()
        {
            CleanupCommandBuffer();
        }

        private void CleanupCommandBuffer()
        {
            renderCamera.RemoveCommandBuffer(CameraEvent.AfterForwardAlpha, commandBuffer);
        }
        
        private void SetupCommandBuffer()
        {
            commandBuffer = new CommandBuffer();
            commandBuffer.Blit(null, BuiltinRenderTextureType.CameraTarget, renderMaterial);
            renderCamera.AddCommandBuffer(CameraEvent.AfterForwardAlpha, commandBuffer);
        }
        
        public void SetBuffer(ComputeBuffer buffer)
        {
            if (!initialized)
            {
                Debug.LogError("Not initialized.");
                return;
            }
            renderMaterial.SetBuffer(Particles, buffer);
        }
    }
}