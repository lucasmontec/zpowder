Shader "Unlit/BufferPixelRenderer"
{
    Properties
    {
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100
        ZTest off
        ZWrite off

        Pass
        {
            CGPROGRAM
// Upgrade NOTE: excluded shader from DX11, OpenGL ES 2.0 because it uses unsized arrays
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "NoisesKejiro.cginc"
                
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2_f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            struct particle_data
             {
                int info;
                float lifetime;
             };
            
            StructuredBuffer<particle_data> _Particles;
            float4 _ParticleColors[256];
            float4 _ParticleLifetimeColors[256];
            
            sampler2D _Source;
            int _Width, _Height;

            uint hash(uint s)
            {
                s ^= 2747636419u;
                s *= 2654435769u;
                s ^= s >> 16;
                s *= 2654435769u;
                s ^= s >> 16;
                s *= 2654435769u;
                return s;
            }

            float random(uint seed)
            {
                return float(hash(seed)) / 4294967295.0; // 2^32-1
            }
            
            v2_f vert (const appdata v)
            {
                v2_f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            float Lighting(int x, int y, float transmission, out float3 emission)
            {
                float ambient = 0;
                const float halfSamples = transmission * 0.5;
                const int lightMul = 2;
                
                for (int j = -halfSamples; j <= halfSamples; j++)
                {
                    for (int k = -halfSamples; k <= halfSamples; k++)
                    {
                        
                        int cx = clamp(x + j * lightMul, 0, _Width - 1);
                        int cy = clamp(y + k * lightMul, 0, _Height - 1);
                        int id = cx + cy * _Width;
                        int t = _Particles[id].info & 0xFF;

                        if (length(_ParticleColors[t]) > 1)
                        {
                            int isLiquid = _Particles[id].info & 1 << 8 ? 2 : 1;
                            float atten = pow(min(1 / distance(float2(x, y), float2(cx, cy)), 0.1), 2);
                            emission += atten * lightMul * _ParticleColors[t] * isLiquid * 1;
                        }

                        int cxAmb = clamp(x + j * 2, 0, _Width - 1);
                        int cyAmb = clamp(y + k + halfSamples, 0, _Height - 1);
                        int idAmb = cxAmb + cyAmb * _Width;
                        int tAmb = _Particles[idAmb].info & 0xFF;
                        
                        ambient += (_ParticleColors[tAmb] > 0) * (_Particles[idAmb].info & 1 << 8 ? 0.5 : 1.5);
                    }
                }

                ambient /= transmission * transmission;
                ambient = pow(saturate(1 - ambient), 2) + 0.05;
                
                return ambient;
            }
            
            fixed4 frag (const v2_f i) : SV_Target
            {
                const int x = int(floor(i.uv.x * _Width));
                const int y = int(floor(i.uv.y * _Height));

                const int pixel_id = y*_Width+x;
                const int type = _Particles[pixel_id].info & 0xFF;
                const int is_liquid = _Particles[pixel_id].info & 1 << 8;
                const int has_lifetime = _Particles[pixel_id].info & 1 << 9;
                const int is_charged = _Particles[pixel_id].info & 1 << 10;
                
                const float lifetime = _Particles[pixel_id].lifetime;

                float4 color;
                float3 emission = 0;
                const float ambient = Lighting(x, y, 20, emission);
                
                //UNITY_BRANCH
                if(is_liquid)
                {
                    if(has_lifetime)
                    {
                       //todo
                        color = lerp(_ParticleColors[type], _ParticleLifetimeColors[type], max(lifetime, 0));
                        const float noise = random(pixel_id) * 0.2;
                        color += color * noise;
                    }
                    else
                    {
                        const float noise = snoise(float3(x, y, 0) * 53.234 + float3(0, 0, _Time.y * 0.8));
                        color = lerp(_ParticleColors[type], _ParticleLifetimeColors[type], noise);
                    }
                }
                else
                {
                    color = lerp(_ParticleColors[type], _ParticleLifetimeColors[type], max(lifetime, 0));
                    const float noise = random(pixel_id) * 0.2;
                    color += color * noise;
                }

                const float3 albedo = color;
                
                color.rgb += albedo * emission;
                color.rgb *= ambient;

                if(is_charged)
                {
                    color.rgb *= 80.0;
                }
                
                if(is_liquid)
                {
                    float flash_noise = snoise(float3(x*0.01, y, 0) * 53.234 + float3(0, 0, _Time.y * 0.04));
                        flash_noise = smoothstep(0.86, 0.95, flash_noise);
                    color += color * flash_noise * smoothstep(0.202, 0.98, ambient) * 16.8;
                }
                
                return color;
            }
            ENDCG
        }
    }
}
