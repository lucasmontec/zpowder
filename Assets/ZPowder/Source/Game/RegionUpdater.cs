using Unity.Mathematics;
using UnityEngine;
using ZPowder.Util;

namespace ZPowder
{
    public class RegionUpdater
    {
        public float RegionSize { get; }
        private readonly bool[,] isDirty;
        private readonly int4[,] dirtyRects;
        public int Width { get; }
        public int Height { get; }
        
        public int DirtyRegionsCount { get; private set; }

        public int4[,] DirtyRects => dirtyRects;
        
        public RegionUpdater(int rawWidth, int rawHeight, float regionSize)
        {
            RegionSize = regionSize;
            
            Width = Mathf.CeilToInt(rawWidth / regionSize);
            Height = Mathf.CeilToInt(rawHeight / regionSize);
            
            isDirty = new bool[Width, Height];
            dirtyRects = new int4[Width, Height];
            ClearDirty();
        }
        
        public bool IsDirty(int regionX, int regionY)
        {
            return isDirty[regionX, regionY];
        }
        
        public int4 GetDirtyRect(int regionX, int regionY)
        {
            return dirtyRects[regionX, regionY];
        }
        
        public void ClearDirty()
        {
            DirtyRegionsCount = 0;
            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    isDirty[x, y] = false;
                    dirtyRects[x, y].Set(0, 0, 0, 0);
                }
            }
        }

        public void SetDirty(int x, int y)
        {
            var regionX = Mathf.FloorToInt(x / RegionSize);
            var regionY = Mathf.FloorToInt(y / RegionSize);
            
            bool prevStatus = isDirty[regionX, regionY];
            isDirty[regionX, regionY] = true;

            if (!prevStatus)
            {
                dirtyRects[regionX, regionY].Set(x, y, x, y);
                DirtyRegionsCount++;
            }
            else
            {
                var curRect = dirtyRects[regionX, regionY];
                int minX = Mathf.Min(x, curRect.x);
                int minY = Mathf.Min(y, curRect.y);
                
                int maxX = Mathf.Max(x, curRect.z);
                int maxY = Mathf.Max(y, curRect.w);
                
                dirtyRects[regionX, regionY].Set(minX, minY, maxX, maxY);
            }
        }
    }
}