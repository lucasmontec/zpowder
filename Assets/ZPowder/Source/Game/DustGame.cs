using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using ZPowder.Particle;

namespace ZPowder
{
    public class DustGame
    {
        public int Width { get; }
        public int Height { get; }

        private ParticleBoard.ParticleBoard board;
        private readonly ParticleBoard.ParticleBoard displayBoard;

        private readonly RegionUpdater regionUpdater;
        
        private readonly Dictionary<ParticleType, DirtyChecker> dirtyChecker = new();
        
        private readonly Dictionary<ParticleType, ParticleUpdater> particleUpdaters = new();

        private readonly HashSet<ParticleUpdater> genericUpdaters = new();
        
        public delegate bool DirtyChecker(int x, int y, float dt, ref ParticleBoard.ParticleBoard board);
        public delegate void ParticleUpdater(long tick, float dt, int x, int y, ref ParticleBoard.ParticleBoard board);

        private const int TicksPerSide = 10;
        private bool rightToLeft;
        public bool UpdateBottomToTop;

        public long Tick { get; private set; }

        public DustGame(int width, int height)
        {
            Width = width;
            Height = height;
            board = new ParticleBoard.ParticleBoard(width, height);
            displayBoard = new ParticleBoard.ParticleBoard(width, height);
            
            regionUpdater = new RegionUpdater(width, height, 32f);
        }
        
        public int RegionColumns()
        {
            return regionUpdater.Width;
        }
        
        public int RegionRows()
        {
            return regionUpdater.Height;
        }
        
        public Particle.Particle this[int x, int y] => board[x, y];
        
        public void AddParticle(int x, int y, Particle.Particle particle)
        {
            board.SetParticle(x, y, Tick-1, particle);
        }

        public void RegisterUpdater(ParticleType type, ParticleUpdater updater)
        {
            particleUpdaters[type] = updater;
        }
        
        public void RegisterDirtyChecker(ParticleType type, DirtyChecker checker)
        {
            dirtyChecker[type] = checker;
        }
        
        public void RegisterGenericUpdater(ParticleUpdater updater)
        {
            genericUpdaters.Add(updater);
        }

        public void Update(float dt)
        {
            if (Tick % TicksPerSide == 0)
            {
                rightToLeft = !rightToLeft;
            }

            void UpdateHorizontal(int y)
            {
                if (rightToLeft)
                {
                    for (int x = 0; x < Width; x++)
                    {
                        UpdatePixel(dt, x, y);
                    }
                }
                else
                {
                    for (int x = Width-1; x >= 0; x--)
                    {
                        UpdatePixel(dt, x, y);
                    }
                }
            }

            if (UpdateBottomToTop)
            {
                for (int y = 0; y < Height; y++)
                {
                    UpdateHorizontal(y);
                }
            }
            else
            {
                for (int y = Height-1; y >= 0; y--)
                {
                    UpdateHorizontal(y);
                }
            }

            board.RunScheduledChanges();
            displayBoard.CopyBoard(board);
            Tick++;
        }

        public void BeginRegionUpdate()
        {
            regionUpdater.ClearDirty();
        }

        public void UpdateRegion(bool leftToRight, bool bottomToTop, float dt, int regionX, int regionY)
        {
            CheckDirty(regionX, regionY, dt);
            if (regionUpdater.IsDirty(regionX, regionY))
            {
                InternalRegionUpdate(leftToRight, bottomToTop, dt, regionUpdater.GetDirtyRect(regionX, regionY));
            }
        }

        private void CheckDirty(int regionX, int regionY, float dt)
        {
            int startX = (int) (regionX * regionUpdater.RegionSize);
            int startY = (int) (regionY * regionUpdater.RegionSize);
            
            int maxX = Mathf.Min((int) (startX + regionUpdater.RegionSize), board.Width);
            int maxY = Mathf.Min((int) (startY + regionUpdater.RegionSize), board.Height);
            
            for (int x = startX; x < maxX; x++)
            {
                for (int y = startY; y < maxY; y++)
                {
                    ref Particle.Particle particle = ref board[x, y];
                    ParticleType particleType = particle.Type;
                    if (dirtyChecker.TryGetValue(particleType, out DirtyChecker checker))
                    {
                        if (checker(x, y, dt, ref board))
                        {
                            regionUpdater.SetDirty(x, y);
                        }
                    }
                }
            }
        }
        
        private void InternalRegionUpdate(bool leftToRight, bool bottomToTop, float dt, int4 regionRect)
        {
            void XUpdate(int y)
            {
                if (leftToRight)
                {
                    for (int x = regionRect.x; x <= regionRect.z; x++)
                    {
                        UpdatePixel(dt, x, y);
                    }
                }
                else
                {
                    for (int x = regionRect.z; x >= regionRect.x; x--)
                    {
                        UpdatePixel(dt, x, y);
                    }
                }
            }

            if (bottomToTop)
            {
                for (int y = regionRect.y; y <= regionRect.w; y++)
                {
                    XUpdate(y);
                }
            }
            else
            {
                for (int y = regionRect.w; y >= regionRect.y; y--)
                {
                    XUpdate(y);
                }
            }
        }

        public void EndUpdate()
        {
            board.RunScheduledChanges();
            displayBoard.CopyBoard(board);
            Tick++;
        }
        
        public void UpdatePixel(float dt, int x, int y)
        {
            ref Particle.Particle particle = ref board[x, y];
            ParticleType particleType = particle.Type;

            if (particleType == ParticleType.Air)
            {
                return;
            }
            
            foreach (ParticleUpdater genericUpdater in genericUpdaters)
            {
                genericUpdater(Tick, dt, x, y, ref board);
            }
            
            if (particle.HasLifetime && particle.LifeTime <= 0)
            {
                board.SetParticle(x, y, Tick, ParticleFactory.Air);
                return;
            }
            
            if (particle.IsUpdated(Tick))
            {
                return;
            }
            
            if (particleUpdaters.TryGetValue(particleType, out ParticleUpdater updater))
            {
                updater(Tick, dt, x, y, ref board);
            }
        }

        public void ClearBoard()
        {
            board.SetAllParticles(Tick, ParticleFactory.Air);
        }
        
        public void FillBoard(Particle.Particle particle)
        {
            board.SetAllParticles(Tick, particle);
        }
    }
}