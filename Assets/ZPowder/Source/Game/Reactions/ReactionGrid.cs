using UnityEngine;
using ZPowder.Particle;

namespace ZPowder.Reactions
{
    public class ReactionGrid
    {
        private readonly int split;
        private readonly int xChunkSize;
        private readonly int yChunkSize;

        private readonly ReactionInfo[,] reactionInfo;
        
        public ReactionGrid(DustGame board, int split)
        {
            this.split = split;
            xChunkSize = Mathf.CeilToInt(1f * board.Width / split);
            yChunkSize = Mathf.CeilToInt(1f * board.Height / split);
            
            reactionInfo = new ReactionInfo[split, split];
        }

        private void Clear()
        {
            for (int y = 0; y < split; y++)
            {
                for (int x = 0; x < split; x++)
                {
                    reactionInfo[x, y].Clear();
                }
            }
        }
        
        public void Process(DustGame board)
        {
            Clear();
            
            for (int y = 0; y < board.Height; y++)
            {
                for (int x = 0; x < board.Width; x++)
                {
                    Particle.Particle particle = board[x, y];
                    int gridX = x / xChunkSize;
                    int gridY = y / yChunkSize;

                    reactionInfo[gridX, gridY].HasFuel |= particle.IsFuel();
                }
            }
        }

        public bool HasFuelNear(int x, int y)
        {
            int gridX = x / xChunkSize;
            int gridY = y / yChunkSize;

            return reactionInfo[gridX, gridY].HasFuel;
        }
    }
}