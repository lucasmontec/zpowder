namespace ZPowder.Reactions
{
    public interface IReactionGridListener
    {
        public void SetReactionGrid(ReactionGrid grid);
    }
}