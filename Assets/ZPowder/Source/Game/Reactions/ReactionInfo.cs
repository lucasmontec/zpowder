namespace ZPowder.Reactions
{
    public struct ReactionInfo
    {
        public bool HasFuel;

        public void Clear()
        {
            HasFuel = false;
        }
    }
}