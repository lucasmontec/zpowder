namespace ZPowder.ParticleBoard
{
    public struct NeighborhoodParticleOperation
    {
        public ParticleOperationTarget Target;
        public Particle.Particle Substitution;
        public Particle.Particle NeighborSubstitution;
        public bool Scheduled { get; private set; }

        public static NeighborhoodParticleOperation None()
        {
            return new()
            {
                Target = ParticleOperationTarget.None
            };
        }
            
        public static NeighborhoodParticleOperation SubstituteSelf(Particle.Particle p, bool scheduled=false)
        {
            return new()
            {
                Target = ParticleOperationTarget.Self,
                Substitution = p,
                Scheduled = scheduled
            };
        }
            
        public static NeighborhoodParticleOperation SubstituteNeighbor(Particle.Particle p, bool scheduled = false)
        {
            return new()
            {
                Target = ParticleOperationTarget.Neighbor,
                NeighborSubstitution = p,
                Scheduled = scheduled
            };
        }
        
        public static NeighborhoodParticleOperation SubstituteSelfAndNeighbor(Particle.Particle self, Particle.Particle neighbor,
            bool scheduled = false)
        {
            return new()
            {
                Target = ParticleOperationTarget.SelfAndNeighbor,
                NeighborSubstitution = neighbor,
                Substitution = self,
                Scheduled = scheduled
            };
        }
            
        public enum ParticleOperationTarget
        {
            None,
            Self,
            Neighbor,
            SelfAndNeighbor
        }
    }
}
    