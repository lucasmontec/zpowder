using Unity.Mathematics;

namespace ZPowder.ParticleBoard
{
    public struct ScheduledParticleChange
    {
        public long tick;
        public int2 position;
        public Particle.Particle particle;

        public static ScheduledParticleChange Create(long tick, int x, int y, Particle.Particle p)
        {
            return new ScheduledParticleChange()
            {
                tick = tick,
                position = new int2(x,y),
                particle = p
            };
        }
    }
}