using System;
using UnityEngine;
using ZPowder.Particle;

namespace ZPowder.ParticleBoard
{
    public class ParticleBoard
    {
        public int Width { get; }
        public int Height { get; }
        
        private readonly Particle.Particle[,] board;

        private readonly Particle.Particle[,] scheduledChanges;
        
        public delegate NeighborhoodParticleOperation NeighborhoodCheckAndChangeMethod(Particle.Particle neighbor, long tick, int neighborX, int neighborY, ParticleBoard board);
        
        public ParticleBoard(int width, int height)
        {
            Width = width;
            Height = height;
            board = new Particle.Particle[width, height];
            
            scheduledChanges = new Particle.Particle[width, height];
            ClearScheduledParticleChanges();
        }
        
        public ref Particle.Particle this[int x, int y] => ref board[x, y];

        public void UpdateParticle(int x, int y, long tick)
        {
            board[x, y].LastUpdateTick = tick;
            if (board[x, y].HasLifetime && board[x, y].LifeTime > 0)
            {
                board[x, y].LifeTime--;
            }
        }

        public void RunScheduledChanges()
        {
            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    Particle.Particle particle = scheduledChanges[x, y];//TODO go jagged
                    if (particle.LastUpdateTick > -1)
                    {
                        board[x, y] = particle;
                    }
                }
            }

            ClearScheduledParticleChanges();
        }
        
        public void SetParticle(int x, int y, long tick, Particle.Particle particle)
        {
            board[x, y] = particle;
            UpdateParticle(x, y, tick);
        }

        public void CopyBoard(ParticleBoard toCopy)
        {
            //TODO: Look for faster copy
            for (int y = 0; y < toCopy.Height; y++)
            {
                for (int x = 0; x < toCopy.Width; x++)
                {
                    board[x, y] = toCopy[x, y];
                }
            }
            
            //board = (Particle[,]) toCopy.board.Clone();
            //Array.Copy(toCopy.board, board, board.Length);
        }
        
        public int CountNeighborhoodType(int x, int y, ParticleType type)
        {
            int count = 0;

            int startY = Mathf.Max(y - 1, 0);
            int endY = Mathf.Min(y + 1, Height - 1);
            
            int startX = Mathf.Max(x - 1, 0);
            int endX = Mathf.Min(x + 1, Width - 1);
            
            for (int nY = startY; nY <= endY; nY++)
            {
                for (int nX = startX; nX <= endX; nX++)
                {
                    if (nX == x && nY == y)
                    {
                        continue;
                    }
                    
                    ref Particle.Particle neighbor = ref board[nX, nY];

                    if (neighbor.Type == type)
                    {
                        count++;
                    }
                }
            }

            return count;
        }

        public int CountMidBottomNeighborhoodType(int x, int y, ParticleType type)
        {
            int count = 0;

            int startY = Mathf.Max(y - 1, 0);
            int endY = Mathf.Min(y, Height - 1);
            
            int startX = Mathf.Max(x - 1, 0);
            int endX = Mathf.Min(x + 1, Width - 1);
            
            for (int nY = startY; nY <= endY; nY++)
            {
                for (int nX = startX; nX <= endX; nX++)
                {
                    if (nX == x && nY == y)
                    {
                        continue;
                    }
                    
                    ref Particle.Particle neighbor = ref board[nX, nY];

                    if (neighbor.Type == type)
                    {
                        count++;
                    }
                }
            }

            return count;
        }
        
        public bool HasMidBottomNeighborhoodType(int x, int y, ParticleType type)
        {
            int startY = Mathf.Max(y - 1, 0);
            int endY = Mathf.Min(y, Height - 1);
            
            int startX = Mathf.Max(x - 1, 0);
            int endX = Mathf.Min(x + 1, Width - 1);
            
            for (int nY = startY; nY <= endY; nY++)
            {
                for (int nX = startX; nX <= endX; nX++)
                {
                    if (nX == x && nY == y)
                    {
                        continue;
                    }
                    
                    ref Particle.Particle neighbor = ref board[nX, nY];

                    if (neighbor.Type == type)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public int CountNeighborhood(int x, int y, Func<Particle.Particle, bool> checkParticle)
        {
            int count = 0;

            int startY = Mathf.Max(y - 1, 0);
            int endY = Mathf.Min(y + 1, Height - 1);
            
            int startX = Mathf.Max(x - 1, 0);
            int endX = Mathf.Min(x + 1, Width - 1);
            
            for (int nY = startY; nY <= endY; nY++)
            {
                for (int nX = startX; nX <= endX; nX++)
                {
                    if (nX == x && nY == y)
                    {
                        continue;
                    }
                    
                    ref Particle.Particle neighbor = ref board[nX, nY];

                    if (checkParticle(neighbor))
                    {
                        count++;
                    }
                }
            }

            return count;
        }
        
        public bool CheckNeighborhood(int x, int y, Func<Particle.Particle, bool> checkParticle)
        {
            for (int nY = y - 1; nY <= y + 1; nY++)
            {
                for (int nX = x - 1; nX <= x + 1; nX++)
                {
                    if (nX == x && nY == y)
                    {
                        continue;
                    }
                    
                    int burnYClamped = Mathf.Clamp(nY, 0, Height - 1);
                    int burnXClamped = Mathf.Clamp(nX, 0, Width - 1);
                    
                    ref Particle.Particle neighbor = ref board[burnXClamped, burnYClamped];

                    return checkParticle(neighbor);
                }
            }

            return false;
        }

        public void CheckAndChangeNeighborhood(int x, int y, long tick, NeighborhoodCheckAndChangeMethod particleChangeMethod)
        {
            CheckAndChangeNeighborhood(false, true, x, y, tick, particleChangeMethod);
        }
        
        public void CheckAndChangeNeighborhood(bool skipUpdated, bool skipSelf, int x, int y, long tick, NeighborhoodCheckAndChangeMethod particleChangeMethod)
        {
            for (int nY = y - 1; nY <= y + 1; nY++)
            {
                for (int nX = x - 1; nX <= x + 1; nX++)
                {
                    if (skipSelf && nX == x && nY == y)
                    {
                        continue;
                    }
                    
                    int nYClamped = Mathf.Clamp(nY, 0, Height - 1);
                    int nXClamped = Mathf.Clamp(nX, 0, Width - 1);

                    ref Particle.Particle neighbor = ref board[nXClamped, nYClamped];
                    
                    if(skipUpdated && neighbor.LastUpdateTick == tick) continue;
                    
                    RunOrScheduleParticleOperation(x, y, tick, particleChangeMethod, neighbor, nXClamped, nYClamped);
                }
            }
        }

        private void RunOrScheduleParticleOperation(int x, int y, long tick, NeighborhoodCheckAndChangeMethod particleChangeMethod,
            Particle.Particle neighbor, int neighborX, int neighborY)
        {
            NeighborhoodParticleOperation operation = particleChangeMethod(neighbor, tick, neighborX, neighborY, this);

            if (operation.Scheduled)
            {
                ScheduleOperation(tick, x, y, neighborX, neighborY, operation);
            }
            else
            {
                if (operation.Target is NeighborhoodParticleOperation.ParticleOperationTarget.Self or
                    NeighborhoodParticleOperation.ParticleOperationTarget.SelfAndNeighbor)
                {
                    SetParticle(x, y, tick, operation.Substitution);
                }

                if (operation.Target is NeighborhoodParticleOperation.ParticleOperationTarget.Neighbor or
                    NeighborhoodParticleOperation.ParticleOperationTarget.SelfAndNeighbor)
                {
                    SetParticle(neighborX, neighborY, tick, operation.NeighborSubstitution);
                }
            }
        }

        private void ScheduleOperation(long tick, int x, int y, int neighborX, int neighborY, NeighborhoodParticleOperation operation)
        {
            if (operation.Target is NeighborhoodParticleOperation.ParticleOperationTarget.Self or 
                NeighborhoodParticleOperation.ParticleOperationTarget.SelfAndNeighbor)
            {
                scheduledChanges[x, y] = operation.Substitution;
                scheduledChanges[x, y].LastUpdateTick = tick;
            }

            if (operation.Target is NeighborhoodParticleOperation.ParticleOperationTarget.Neighbor or
                NeighborhoodParticleOperation.ParticleOperationTarget.SelfAndNeighbor)
            {
                scheduledChanges[neighborX, neighborY] = operation.NeighborSubstitution;
                scheduledChanges[neighborX, neighborY].LastUpdateTick = tick;
            }
        }
        
        public void SetAllParticles(long tick, Particle.Particle particle)
        {
            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    SetParticle(x, y, tick, particle);
                }
            }

            ClearScheduledParticleChanges();
        }

        private void ClearScheduledParticleChanges()
        {
            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    scheduledChanges[x, y] = ParticleFactory.Air;
                    scheduledChanges[x, y].LastUpdateTick = -1;
                }
            }
        }
    }
}