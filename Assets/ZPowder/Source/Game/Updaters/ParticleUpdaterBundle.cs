using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using ZPowder.Particle;
using ZPowder.Updaters.Base;

namespace ZPowder.Updaters
{
    [CreateAssetMenu(menuName = Constants.Constants.ZPowderMenuBase + "Particle Definition Bundle")]
    public class ParticleUpdaterBundle : SerializedScriptableObject
    {
        // ReSharper disable once CollectionNeverUpdated.Global
        public readonly Dictionary<ParticleType, ParticleDefinition> ParticleDefinitions =
            new();

        [OdinSerialize, NonSerialized]
        public readonly List<AbstractGenericParticleUpdater> GenericParticleUpdaters =
            new();
    }
}