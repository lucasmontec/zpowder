using Sirenix.OdinInspector;

namespace ZPowder.Updaters.Base
{
    public abstract class AbstractParticleUpdater : SerializedScriptableObject, IParticleUpdater
    {
        public abstract void UpdateParticle(long tick, float dt, int x, int y, ref ParticleBoard.ParticleBoard board);
        public abstract bool CheckDirty(int px, int py, float dt, ref ParticleBoard.ParticleBoard board);
    }
}