namespace ZPowder.Updaters.Base
{
    public interface IGenericParticleUpdater
    {
        void UpdateParticle(long tick, float dt, int x, int y, ref ParticleBoard.ParticleBoard board);
    }
}