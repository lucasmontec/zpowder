using Sirenix.OdinInspector;

namespace ZPowder.Updaters.Base
{
    public abstract class AbstractGenericParticleUpdater : SerializedScriptableObject, IGenericParticleUpdater
    {
        public abstract void UpdateParticle(long tick, float dt, int x, int y, ref ParticleBoard.ParticleBoard board);
    }
}