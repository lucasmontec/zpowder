namespace ZPowder.Updaters.Base
{
    public interface IParticleUpdater : IGenericParticleUpdater
    {
        bool CheckDirty(int px, int py, float dt, ref ParticleBoard.ParticleBoard board);
    }
}