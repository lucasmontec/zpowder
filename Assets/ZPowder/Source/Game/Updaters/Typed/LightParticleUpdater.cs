using UnityEngine;
using ZPowder.Particle;
using ZPowder.Threading;
using ZPowder.Updaters.Base;

namespace ZPowder.Updaters.Typed
{
    [CreateAssetMenu(menuName = Constants.Constants.ZPowderMenuBase + "Particle Updaters/Light Updater")]
    public class LightParticleUpdater : AbstractParticleUpdater
    {
        public bool canDamageParticles;
        
        public override void UpdateParticle(long tick, float dt, int x, int y, ref ParticleBoard.ParticleBoard board)
        {
            Particle.Particle particle = board[x, y];
            board.UpdateParticle(x, y, tick);
            
            if (x == 0 || x == board.Width - 1)
            {
                board.SetParticle(x, y, tick, ParticleFactory.Air);
                return;
            }
                        
            if (y == 0 || y == board.Height - 1)
            {
                board.SetParticle(x, y, tick, ParticleFactory.Air);
                return;
            }
            
            int nextX = Mathf.Clamp((int)(x + particle.Velocity.x * dt), 0, board.Width-1);
            int nextY = Mathf.Clamp((int)(y + particle.Velocity.y * dt), 0, board.Height-1);

            int lastX = x;
            int lastY = y;
            
            int deltaX = nextX - x;
            int deltaY = nextY - y;

            int dirX = deltaX > 0 ? 1 : -1;
            int dirY = deltaY > 0 ? 1 : -1;

            if (deltaX == 0 && deltaY == 0)
            {
                return;
            }
            
            for (int yTrace = 0; yTrace <= Mathf.Abs(deltaY); yTrace++)
            {
                int realY = y + yTrace*dirY;
                for (int xTrace = 0; xTrace <= Mathf.Abs(deltaX); xTrace++)
                {
                    int realX = x + xTrace*dirX;

                    if (realX == x && realY == y)
                    {
                        continue; 
                    }
                    
                    ref Particle.Particle particleOnDesiredPos = ref board[realX, realY];

                    if (particleOnDesiredPos.IsAir())
                    {
                        board.SetParticle(lastX, lastY, tick, ParticleFactory.Air);
                        board.SetParticle(realX, realY, tick, particle);
                        
                        lastX = realX;
                        lastY = realY;
                        continue;
                    }

                    if (ParticleUpdaterUtils.CanDestroyAtSpace(lastX, lastY, realX, realY, ref board))
                    {
                        board.SetParticle(lastX, lastY, tick, ParticleFactory.Air);
                        particle.LifeTime = 1;

                        if (canDamageParticles)
                        {
                            if (particleOnDesiredPos.LifeTime == 0)
                            {
                                particleOnDesiredPos.LifeTime = particleOnDesiredPos.Hardness();
                                particleOnDesiredPos.SetDamaged();
                            }

                            if (particleOnDesiredPos.LifeTime == 1 || particleOnDesiredPos.HasLifetime)
                            {
                                board.SetParticle(realX, realY, tick, particle);
                            }
                            else
                            {
                                particleOnDesiredPos.LifeTime--;
                            }
                        }
                    }
                    else
                    {
                        Particle.Particle toSet = ParticleFactory.Air;

                        if (particleOnDesiredPos.Type == particle.Type && Vector2.Dot(particleOnDesiredPos.Velocity, particle.Velocity) >= 0.95f)
                        {
                            toSet = particle;
                        }
                        
                        board.SetParticle(lastX, lastY, tick, toSet);
                        return;
                    }
                    
                    lastX = realX;
                    lastY = realY;
                }
            }
        }

        public override bool CheckDirty(int px, int py, float dt, ref ParticleBoard.ParticleBoard board)
        {
            return DirtyCheckers.VelocityDirectionSpaceDirty(px, py, dt, ref board);
        }
        
    }
}