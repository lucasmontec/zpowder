using UnityEngine;
using ZPowder.Constants;
using ZPowder.Threading;
using ZPowder.Updaters.Base;

namespace ZPowder.Updaters.Typed
{
    [CreateAssetMenu(menuName = Constants.Constants.ZPowderMenuBase + "Particle Updaters/Powder Updater")]
    public class PowderParticleUpdater : AbstractParticleUpdater
    {
        private static readonly ParticleUpdaterUtils.TraceYMethod TraceStraightYThenDiagonalsDelegate = ParticleUpdaterUtils.TraceStraightYThenDiagonals;
        private static readonly ParticleUpdaterUtils.CheckFreeSlotMethod CanOccupySpaceDelegate = ParticleUpdaterUtils.CanOccupySpace;
        
        public override void UpdateParticle(long tick, float dt, int x, int y, ref ParticleBoard.ParticleBoard board)
        {
            Particle.Particle particle = board[x, y];
            
            particle.Velocity += PowderPhysics.Gravity * dt;
            
            int desiredY = (int)(y + particle.Velocity.y * dt);

            (int selectedX, int selectedY, bool hit) = ParticleUpdaterUtils.TraceY(x, y, desiredY, ref board, CanOccupySpaceDelegate, TraceStraightYThenDiagonalsDelegate);

            if (hit)
            {
                particle.Velocity.y = 0;
            }
            
            board.SetParticle(x, y, tick, board[selectedX, selectedY]);
            board.SetParticle(selectedX, selectedY, tick, particle);
        }

        public override bool CheckDirty(int px, int py, float dt, ref ParticleBoard.ParticleBoard board)
        {
            return DirtyCheckers.BottomSpaceDirty(px, py, dt, ref board);
        }
    }
}