using System;
using UnityEngine;
using ZPowder.Particle;
using ZPowder.Updaters.Base;

namespace ZPowder.Updaters.Typed
{
    [CreateAssetMenu(menuName = Constants.Constants.ZPowderMenuBase + "Particle Updaters/Energy Updater")]
    public class EnergyUpdater : AbstractParticleUpdater
    {
        private static Unity.Mathematics.Random _random =
            new((uint) Guid.NewGuid().GetHashCode());

        public float lifetimeLossOnPath = 0.7f;
        public float randomVelocityRange = 1000f;
        public float spawnSideParticleChance = 0.06f;

        public override void UpdateParticle(long tick, float dt, int x, int y, ref ParticleBoard.ParticleBoard board)
        {
            Particle.Particle particle = board[x, y];
            board.UpdateParticle(x, y, tick);
            
            if (x == 0 || x == board.Width - 1)
            {
                board.SetParticle(x, y, tick, ParticleFactory.Air);
                return;
            }
                        
            if (y == 0 || y == board.Height - 1)
            {
                board.SetParticle(x, y, tick, ParticleFactory.Air);
                return;
            }

            int nextX = Mathf.Clamp((int)(x + particle.Velocity.x * dt), 0, board.Width-1);
            int nextY = Mathf.Clamp((int)(y + particle.Velocity.y * dt), 0, board.Height-1);

            int lastX = x;
            int lastY = y;
            
            int deltaX = nextX - x;
            int deltaY = nextY - y;

            int dirX = deltaX > 0 ? 1 : -1;
            int dirY = deltaY > 0 ? 1 : -1;

            if (deltaX == 0 && deltaY == 0)
            {
                return;
            }

            if (Vector3.SqrMagnitude(particle.Velocity) > 0)
            {
                if (Mathf.Abs(particle.Velocity.x) > Mathf.Abs(particle.Velocity.y))
                {
                    particle.Velocity.y += _random.NextFloat(-randomVelocityRange, randomVelocityRange) * dt;
                }
                else
                {
                    particle.Velocity.x += _random.NextFloat(-randomVelocityRange, randomVelocityRange) * dt;
                }
            }

            for (int yTrace = 0; yTrace <= Mathf.Abs(deltaY); yTrace++)
            {
                int realY = y + yTrace*dirY;
                for (int xTrace = 0; xTrace <= Mathf.Abs(deltaX); xTrace++)
                {
                    int realX = x + xTrace*dirX;

                    if (realX == x && realY == y)
                    {
                        continue; 
                    }
                    
                    ref Particle.Particle particleOnDesiredPos = ref board[realX, realY];

                    if (particleOnDesiredPos.IsAir())
                    {
                        if (particle.NormalizedLifetime < 1)
                        {
                            var toPlaceOnLastPos = ParticleFactory.FromType(particle.Type);
                            toPlaceOnLastPos.LifeTime = (int) (particle.LifeTime * lifetimeLossOnPath);

                            if (_random.NextFloat() < spawnSideParticleChance)
                            {
                                toPlaceOnLastPos.Velocity = _random.NextFloat() < 0.5f ? 
                                    new Vector2(particle.Velocity.y,-particle.Velocity.x) : 
                                    new Vector2(-particle.Velocity.y,particle.Velocity.x);
                            }
                            else
                            {
                                toPlaceOnLastPos.Velocity = Vector2.zero;
                            }
                            
                            board.SetParticle(lastX, lastY, tick, toPlaceOnLastPos);
                        }
                        else
                        {
                            board.SetParticle(lastX, lastY, tick, ParticleFactory.Air);
                        }

                        //board.SetParticle(lastX, lastY, tick, ParticleFactory.Air);
                        board.SetParticle(realX, realY, tick, particle);
                        
                        lastX = realX;
                        lastY = realY;
                        continue;
                    }

                    if (particleOnDesiredPos.IsConductive())
                    {
                        break;
                    }

                    Particle.Particle toSet = ParticleFactory.Air;

                    if (particleOnDesiredPos.Type == particle.Type && Vector2.Dot(particleOnDesiredPos.Velocity, particle.Velocity) >= 0.95f)
                    {
                        toSet = particle;
                    }
                    
                    board.SetParticle(lastX, lastY, tick, toSet);

                    lastX = realX;
                    lastY = realY;
                }
            }
        }

        public override bool CheckDirty(int px, int py, float dt, ref ParticleBoard.ParticleBoard board)
        {
            return true;
        }
    }
}