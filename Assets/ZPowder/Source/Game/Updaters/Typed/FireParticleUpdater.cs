using UnityEngine;
using ZPowder.Threading;
using ZPowder.Updaters.Base;

namespace ZPowder.Updaters.Typed
{
    [CreateAssetMenu(menuName = Constants.Constants.ZPowderMenuBase + "Particle Updaters/Fire Updater")]
    public class FireParticleUpdater : AbstractParticleUpdater
    {
        private static readonly ParticleUpdaterUtils.CheckFreeSlotMethod CanOccupySpaceAirOnlyDelegate = ParticleUpdaterUtils.CanOccupySpaceAirOnly;
        private static readonly ParticleUpdaterUtils.TraceYMethod TraceStraightYThenDiagonalsDelegate = ParticleUpdaterUtils.TraceStraightYThenDiagonals;
        
        public override void UpdateParticle(long tick, float dt, int x, int y, ref ParticleBoard.ParticleBoard board)
        {
            Particle.Particle particle = board[x, y];

            if (particle.Velocity.y == 0)
            {
                particle.Velocity.y -= 80f;
            }
            if (Mathf.Abs(particle.Velocity.y) <= 170)
            {
                particle.Velocity.y -= 3f;
            }
            
            int desiredY = (int)(y - particle.Velocity.y * dt);

            (int selectedX, int selectedY, bool _) = 
                ParticleUpdaterUtils.TraceY(x, y, desiredY, ref board, CanOccupySpaceAirOnlyDelegate, TraceStraightYThenDiagonalsDelegate);
            
            if (selectedY != y)
            {
                particle.Velocity.x = 0;
                board.SetParticle(x, y, tick, board[selectedX, selectedY]);
                board.SetParticle(selectedX, selectedY, tick, particle);
                return;
            }

            if (particle.Velocity.x == 0)
            {
                particle.Velocity.x = ThreadSafeRandom.Value > 0.5f ? 0.7f : -0.7f;
            }
            else if (Mathf.Abs(particle.Velocity.x) < 4f)
            {
                particle.Velocity.x *= 1.2f;
            }

            int particleXVelocity = (int)particle.Velocity.x;
            
            int desiredX = x + particleXVelocity;
            bool hit;
            (selectedX, selectedY, hit) = ParticleUpdaterUtils.TraceXAndCompensateY(x, y, desiredX, ref board, CanOccupySpaceAirOnlyDelegate, true);

            if (hit)
            {
                particle.Velocity.x *= -1;
                particle.Velocity.x *= 0.7f;
            }

            board.SetParticle(x, y, tick, board[selectedX, selectedY]);
            board.SetParticle(selectedX, selectedY, tick, particle);
        }

        public override bool CheckDirty(int px, int py, float dt, ref ParticleBoard.ParticleBoard board)
        {
            return board[px, py].HasLifetime;
        }
    }
}