using UnityEngine;
using ZPowder.Constants;
using ZPowder.Particle;
using ZPowder.Threading;
using ZPowder.Updaters.Base;

namespace ZPowder.Updaters.Typed
{
    [CreateAssetMenu(menuName = Constants.Constants.ZPowderMenuBase + "Particle Updaters/Liquid Updater")]
    public class LiquidParticleUpdater : AbstractParticleUpdater
    {
        private static readonly ParticleUpdaterUtils.TraceYMethod TraceStraightYThenDiagonalsDelegate = ParticleUpdaterUtils.TraceStraightYThenDiagonals;
        private static readonly ParticleUpdaterUtils.CheckFreeSlotMethod CanOccupySpaceDelegate = ParticleUpdaterUtils.CanOccupySpace;
        
        public int viscosityBase = 200;
        public float normalizedViscosityMultiplier = 25f;
        
        public float xVelocityMultiplier = 20f;
        public float particleHitVelocityMultiplier = 0.4f;
        
        public override void UpdateParticle(long tick, float dt, int x, int y, ref ParticleBoard.ParticleBoard board)
        {
            Particle.Particle particle = board[x, y];
            
            if (board.CountMidBottomNeighborhoodType(x, y, particle.Type) == 5)
            {
                board.SetParticle(x, y, tick, particle);
                return;
            }
                
            particle.Velocity += PowderPhysics.Gravity * dt;
            
            int desiredY = (int)(y + particle.Velocity.y * dt);

            (int selectedX, int selectedY, bool hitBottom) = ParticleUpdaterUtils.TraceY(x, y, desiredY, ref board, CanOccupySpaceDelegate, TraceStraightYThenDiagonalsDelegate);

            if (hitBottom)
            {
                particle.Velocity.y = 0;
            }
            
            if (selectedY != y)
            {
                board.SetParticle(x, y, tick, board[selectedX, selectedY]);
                board.SetParticle(selectedX, selectedY, tick, particle);
                return;
            }

            int normalizedViscosity = viscosityBase - particle.Viscosity();
            if (particle.Velocity.x == 0)
            {
                particle.Velocity.x = (ThreadSafeRandom.Value > 0.5f ? 1 : -1) * normalizedViscosity * dt * xVelocityMultiplier;
            }
            else
            {
                int maxSidewaysVelocity = Mathf.Min(normalizedViscosity * 3, PowderPhysics.FinalVelocity);
                if (Mathf.Abs(particle.Velocity.x) < maxSidewaysVelocity)
                {
                    particle.Velocity.x += (particle.Velocity.x > 0? 1 : -1) * normalizedViscosity * normalizedViscosityMultiplier * dt;
                }
            }

            int desiredX = (int)(x + particle.Velocity.x * dt);
            bool hit;
            (selectedX, selectedY, hit) = ParticleUpdaterUtils.TraceXAndCompensateY(x, y, desiredX, ref board, CanOccupySpaceDelegate);

            if (hit)
            {
                particle.Velocity.x *= -1;
                particle.Velocity.x *= particleHitVelocityMultiplier;
            }
            
            board.SetParticle(x, y, tick, board[selectedX, selectedY]);
            board.SetParticle(selectedX, selectedY, tick, particle);
        }

        public override bool CheckDirty(int px, int py, float dt, ref ParticleBoard.ParticleBoard board)
        {
            return DirtyCheckers.MidBottomSpaceDirty(px, py, dt, ref board) || board[px, py].IsCharged();
        }
    }
}