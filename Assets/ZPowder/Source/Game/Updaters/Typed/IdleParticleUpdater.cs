using UnityEngine;
using ZPowder.Particle;
using ZPowder.Updaters.Base;

namespace ZPowder.Updaters.Typed
{
    [CreateAssetMenu(menuName = Constants.Constants.ZPowderMenuBase + "Particle Updaters/Idle Updater")]
    public class IdleParticleUpdater : AbstractParticleUpdater
    {
        public override void UpdateParticle(long tick, float dt, int x, int y, ref ParticleBoard.ParticleBoard board)
        {
            board.UpdateParticle(x, y, tick);
        }

        public override bool CheckDirty(int px, int py, float dt, ref ParticleBoard.ParticleBoard board)
        {
            return board[px, py].HasLifetime || board[px, py].IsCharged();
        }
    }
}