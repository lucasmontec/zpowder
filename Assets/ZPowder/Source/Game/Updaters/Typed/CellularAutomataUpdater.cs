using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using ZPowder.Particle;
using ZPowder.ParticleBoard;
using ZPowder.Threading;
using ZPowder.Updaters.Base;
using Random = System.Random;

namespace ZPowder.Updaters.Typed
{
    [CreateAssetMenu(menuName = Constants.Constants.ZPowderMenuBase + "Particle Updaters/Cellular Automata Updater")]
    public class CellularAutomataUpdater : AbstractParticleUpdater
    {
        public enum RuleOperation
        {
            Keep,
            Create
        }
        
        [Serializable]
        public struct RuleResult
        {
            [ShowIf(nameof(operation), RuleOperation.Create)]
            public ParticleType type;
            public RuleOperation operation;
        }
        
        [OdinSerialize, NonSerialized]
        public Dictionary<int, RuleResult> ruleSet = new();

        [SerializeField]
        private ParticleType neighborCountType = ParticleType.Automata;

        [SerializeField, Tooltip("Type used when no rule is defined")]
        private RuleResult fallbackRule;

        private static readonly ParticleBoard.ParticleBoard.NeighborhoodCheckAndChangeMethod CanOccupySpaceAirOnlyDelegate;
        
        public override void UpdateParticle(long tick, float dt, int x, int y, ref ParticleBoard.ParticleBoard board)
        {
            if (!ruleSet.Any())
            {
                return;
            }

            int fireNeighborCount = board.CountNeighborhoodType(x, y, ParticleType.Fire);
            if (fireNeighborCount > 0)
            {
                board.SetParticle(x, y, tick, ParticleFactory.Air);
                return;
            }
            
            board.CheckAndChangeNeighborhood(true, false, x, y, tick, AutomataNeighborCheck);
        }

        private NeighborhoodParticleOperation AutomataNeighborCheck(Particle.Particle particle, long tick, int neighborX, int neighborY, ParticleBoard.ParticleBoard board)
        {
            board.UpdateParticle(neighborX, neighborY, tick);
            int neighborhoodCount = board.CountNeighborhoodType(neighborX, neighborY, neighborCountType);

            if (!ruleSet.ContainsKey(neighborhoodCount))
                return GetFallbackOperation();
            
            RuleResult result = ruleSet[neighborhoodCount];

            return GetRuleOperation(result);
        }

        private static NeighborhoodParticleOperation GetRuleOperation(RuleResult result)
        {
            return result.operation switch
            {
                RuleOperation.Create => NeighborhoodParticleOperation.SubstituteNeighbor(
                    ParticleFactory.FromType(result.type), true),
                RuleOperation.Keep => NeighborhoodParticleOperation.None(),
                _ => throw new ArgumentOutOfRangeException()
            };
        }

        private NeighborhoodParticleOperation GetFallbackOperation()
        {
            return fallbackRule.operation == RuleOperation.Keep
                ? NeighborhoodParticleOperation.None()
                : NeighborhoodParticleOperation.SubstituteNeighbor(ParticleFactory.FromType(fallbackRule.type), true);
        }

        public override bool CheckDirty(int px, int py, float dt, ref ParticleBoard.ParticleBoard board)
        {
            //TODO implement automata stability based dirty checker
            //Check if the region configuration is stable (was kept for some frames)

            return true;
            
            //return ThreadSafeRandom.Value < 0.01f;
        }
    }
}
