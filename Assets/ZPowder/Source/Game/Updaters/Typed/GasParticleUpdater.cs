using UnityEngine;
using ZPowder.Particle;
using ZPowder.Threading;
using ZPowder.Updaters.Base;

namespace ZPowder.Updaters.Typed
{
    [CreateAssetMenu(menuName = Constants.Constants.ZPowderMenuBase + "Particle Updaters/Gas Updater")]
    public class GasParticleUpdater : AbstractParticleUpdater
    {
        private static readonly ParticleUpdaterUtils.CheckFreeSlotMethod CanOccupySpaceAirOnlyDelegate = ParticleUpdaterUtils.CanOccupySpaceAirOnly;
        private static readonly ParticleUpdaterUtils.TraceYMethod TraceRandomYDelegate = ParticleUpdaterUtils.TraceYRandomStrategy;
        
        public override void UpdateParticle(long tick, float dt, int x, int y, ref ParticleBoard.ParticleBoard board)
        {
            Particle.Particle particle = board[x, y];

            if (particle.Velocity.y == 0)
            {
                particle.Velocity.y += (particle.Volatility() - 16) * 200 * dt;
            }
            if (Mathf.Abs(particle.Velocity.y) <= particle.Volatility() * 500)
            {
                particle.Velocity.y += particle.Volatility() * 20 * dt;
            }
            
            int desiredY = (int)(y + particle.Velocity.y * dt);

            (int selectedX, int selectedY, bool hitBottom) = ParticleUpdaterUtils.TraceY(x, y, desiredY, ref board, CanOccupySpaceAirOnlyDelegate, TraceRandomYDelegate);

            if (hitBottom)
            {
                particle.Velocity.y *= 0.25f;
            }

            var scaledVolatility = particle.Volatility() * 3;
            
            if (particle.Velocity.x == 0)
            {
                particle.Velocity.x = (ThreadSafeRandom.Value > 0.5f ? scaledVolatility : -scaledVolatility) * dt * 4;
            } else if (Mathf.Abs(particle.Velocity.x) < scaledVolatility * 3)
            {
                if (particle.Velocity.x > 0)
                {
                    particle.Velocity.x += dt * scaledVolatility;
                }
                else
                {
                    particle.Velocity.x -= dt * scaledVolatility;
                }
            }
            
            int desiredX = (int)(x + particle.Velocity.x * dt);
            
            bool hit;
            (selectedX, selectedY, hit) = ParticleUpdaterUtils.TraceX(x, y, selectedX, selectedY, desiredX, ref board, CanOccupySpaceAirOnlyDelegate);

            bool randomGasSideSwitch = particle.LifeTime % 6 == 0 && ThreadSafeRandom.Value < 0.2f;
            if (hit || randomGasSideSwitch)
            {
                particle.Velocity.x *= -1;
                particle.Velocity.x *= 0.9f;
            }

            board.SetParticle(x, y, tick, board[selectedX, selectedY]);
            board.SetParticle(selectedX, selectedY, tick, particle);
        }

        public override bool CheckDirty(int px, int py, float dt, ref ParticleBoard.ParticleBoard board)
        {
            return DirtyCheckers.MidTopSpaceDirty(px, py, dt, ref board);
        }
    }
}