using UnityEngine;
using ZPowder.Particle;
using ZPowder.Threading;


namespace ZPowder.Updaters
{
    public static class ParticleUpdaterUtils
    {
        public delegate bool CheckFreeSlotMethod(int x, int y, int nextX, int nextY, ref ParticleBoard.ParticleBoard board);

        public delegate bool TraceYMethod(ref ParticleBoard.ParticleBoard particleBoard, int yTrace,
            CheckFreeSlotMethod isSlotFree, int x, int y, ref int selectedY, ref bool hitStraightScan,
            ref int selectedX);

        /// <summary>
        /// Traces sideways and goes down 1 block on side trace.
        /// If side trace detects a free space up or down, it moves to it.
        /// To go up set compensateYUp to true, it goes down otherwise.
        /// </summary>
        public static (int selectedX, int selectedY, bool hit) TraceXAndCompensateY(int x, int y, int desiredX, ref ParticleBoard.ParticleBoard board, CheckFreeSlotMethod checkFreeSlotMethod, bool compensateYUp=false)
        {
            bool hit = false;

            int selectedX = x, selectedY = y;
            
            if (desiredX == x)
            {
                return (selectedX, selectedY, false);
            }
            
            bool Trace(ref ParticleBoard.ParticleBoard particleBoard, int xTrace)
            {
                //Clamp
                if (xTrace < 0)
                {
                    selectedX = 0;
                    hit = true;
                    return true;
                }
                if (xTrace >= particleBoard.Width)
                {
                    selectedX = particleBoard.Width-1;
                    hit = true;
                    return true;
                }

                //Trace side
                if (checkFreeSlotMethod(x, y, xTrace, selectedY, ref particleBoard))
                {
                    selectedX = xTrace;
                }
                else
                {
                    hit = true;
                }

                if (compensateYUp)
                {
                    //Go down
                    if (selectedY < particleBoard.Height-1 && checkFreeSlotMethod(x, y, xTrace, selectedY + 1, ref particleBoard))
                    {
                        selectedY += 1;
                    }
                }
                else
                {
                    //Go down
                    if (selectedY > 0 && checkFreeSlotMethod(x, y, xTrace, selectedY - 1, ref particleBoard))
                    {
                        selectedY -= 1;
                    } 
                }
                
                return hit;
            }

            if (desiredX < x)
            {
                for (int xTrace = x - 1; xTrace >= desiredX; xTrace--)
                {
                    if (Trace(ref board, xTrace)) break;
                }
            }
            else if (desiredX > x)
            {
                for (int xTrace = x + 1; xTrace <= desiredX; xTrace++)
                {
                    if (Trace(ref board, xTrace)) break;
                }
            }

            return (selectedX, selectedY, hit);
        }

        public static (int selectedX, int selectedY, bool hit) TraceX(int x, int y, int selectedX, int selectedY, int desiredX, ref ParticleBoard.ParticleBoard board, CheckFreeSlotMethod checkFreeSlotMethod)
        {
            bool hit = false;

            if (desiredX == x)
            {
                return (selectedX, selectedY, false);
            }
            
            bool Trace(ref ParticleBoard.ParticleBoard particleBoard, int xTrace)
            {
                //Clamp
                if (xTrace < 0)
                {
                    selectedX = 0;
                    hit = true;
                    return true;
                }
                if (xTrace >= particleBoard.Width)
                {
                    selectedX = particleBoard.Width-1;
                    hit = true;
                    return true;
                }

                //Trace side
                if (checkFreeSlotMethod(x, y, xTrace, selectedY, ref particleBoard))
                {
                    selectedX = xTrace;
                }
                else
                {
                    hit = true;
                }
                
                return false;
            }

            if (desiredX < x)
            {
                for (int xTrace = x - 1; xTrace >= desiredX; xTrace--)
                {
                    if (Trace(ref board, xTrace)) break;
                }
            }
            else if (desiredX > x)
            {
                for (int xTrace = x + 1; xTrace <= desiredX; xTrace++)
                {
                    if (Trace(ref board, xTrace)) break;
                }
            }

            return (selectedX, selectedY, hit);
        }

        /// <summary>
        /// Scans trying to go down first than sideways. Tries to go down to the desired Y.
        /// </summary>
        public static (int selectedX, int selectedY, bool hitStraightScan) TraceY(int x, int y, int desiredY, ref ParticleBoard.ParticleBoard board, CheckFreeSlotMethod isSlotFree, TraceYMethod traceMethod)
        {
            desiredY = Mathf.Clamp(desiredY, 0, board.Height - 1);
            
            int selectedX = x;
            int selectedY = y;
            bool hitStraightScan = false;

            if (desiredY == y)
            {
                return (selectedX, selectedY, false);
            }

            if (desiredY < y)
            {
                for (int yTrace = y - 1; yTrace >= desiredY; yTrace--)
                {
                    if (traceMethod(ref board, yTrace, isSlotFree, x, y, ref selectedY, ref hitStraightScan, ref selectedX))
                        break;
                }
            }
            else if(desiredY > y)
            {
                for (int yTrace = y + 1; yTrace <= desiredY; yTrace++)
                {
                    if (traceMethod(ref board, yTrace, isSlotFree, x, y, ref selectedY, ref hitStraightScan, ref selectedX))
                        break;
                }
            }

            return (selectedX, selectedY, hitStraightScan);
        }

        public static bool TraceStraightYThenDiagonals(ref ParticleBoard.ParticleBoard particleBoard, int yTrace, CheckFreeSlotMethod isSlotFree, int x, int y, ref int selectedY, ref bool hitStraightScan, ref int selectedX)
        {
            //Straight
            if (isSlotFree(x, y, x, yTrace, ref particleBoard))
            {
                selectedY = yTrace;
                return false;
            }

            hitStraightScan = true;

            //Sides
            if (selectedX - 1 >= 0 && isSlotFree(x, y, selectedX - 1, yTrace, ref particleBoard))
            {
                selectedX -= 1;
                selectedY = yTrace;
                return false;
            }

            if (selectedX + 1 < particleBoard.Width && isSlotFree(x, y, selectedX + 1, yTrace, ref particleBoard))
            {
                selectedX += 1;
                selectedY = yTrace;
            }

            return true;
        }

        public static bool TraceYRandomStrategy(ref ParticleBoard.ParticleBoard particleBoard, int yTrace, CheckFreeSlotMethod isSlotFree, int x, int y, ref int selectedY, ref bool hitStraightScan, ref int selectedX)
        {
            void ScanRight(ref ParticleBoard.ParticleBoard pBoard, ref int sY, ref int sX)
            {
                if (sX + 1 < pBoard.Width && isSlotFree(x, y, sX + 1, yTrace, ref pBoard))
                {
                    sX += 1;
                    sY = yTrace;
                }
            }

            void ScanLeft(ref ParticleBoard.ParticleBoard pBoard, ref int sY, ref int sX)
            {
                if (sX - 1 >= 0 && isSlotFree(x, y, sX - 1, yTrace, ref pBoard))
                {
                    sX -= 1;
                    sY = yTrace;
                }
            }

            void ScanStraight(ref ParticleBoard.ParticleBoard board, ref int sY, ref bool hit)
            {
                if (isSlotFree(x, y, x, yTrace, ref board))
                {
                    sY = yTrace;
                }
                else
                {
                    hit = true;
                }
            }

            var randomNumber = ThreadSafeRandom.Value;
            if (randomNumber < 0.33f)
            {
                ScanStraight(ref particleBoard, ref selectedY, ref hitStraightScan);
                ScanLeft(ref particleBoard, ref selectedY, ref selectedX);
                ScanRight(ref particleBoard, ref selectedY, ref selectedX);
            }
            else if (randomNumber >= 0.33f && randomNumber < 0.66f)
            {
                ScanLeft(ref particleBoard, ref selectedY, ref selectedX);
                ScanRight(ref particleBoard, ref selectedY, ref selectedX);
                ScanStraight(ref particleBoard, ref selectedY, ref hitStraightScan);
            }
            else
            {
                ScanRight(ref particleBoard, ref selectedY, ref selectedX);
                ScanLeft(ref particleBoard, ref selectedY, ref selectedX);
                ScanStraight(ref particleBoard, ref selectedY, ref hitStraightScan);
            }

            //Straight
            if (isSlotFree(x, y, x, yTrace, ref particleBoard))
            {
                selectedY = yTrace;
                return false;
            }

            hitStraightScan = true;

            //Sides
            if (selectedX - 1 >= 0 && isSlotFree(x, y, selectedX - 1, yTrace, ref particleBoard))
            {
                selectedX -= 1;
                selectedY = yTrace;
                return false;
            }

            if (selectedX + 1 < particleBoard.Width && isSlotFree(x, y, selectedX + 1, yTrace, ref particleBoard))
            {
                selectedX += 1;
                selectedY = yTrace;
            }

            return true;
        }

        public static bool CanDestroyAtSpace(int x, int y, int nextX, int nextY, ref ParticleBoard.ParticleBoard board)
        {
            ref Particle.Particle particleOnDesiredPos = ref board[nextX, nextY];
            ref Particle.Particle currentParticle = ref board[x, y];

            if (particleOnDesiredPos.Type == currentParticle.Type) 
                return false;
            
            return true;
        }
        
        public static bool CanOccupySpace(int x, int y, int nextX, int nextY, ref ParticleBoard.ParticleBoard board)
        {
            ref Particle.Particle particleOnDesiredPos = ref board[nextX, nextY];
            ref Particle.Particle currentParticle = ref board[x, y];

            if (particleOnDesiredPos.Type == currentParticle.Type) 
                return false;
            
            bool isNextAir = particleOnDesiredPos.IsAir();
            bool isNextLiquid = particleOnDesiredPos.IsLiquid();
            bool isCurrentParticleLiquid = currentParticle.IsLiquid();

            if (!isNextLiquid && (!isCurrentParticleLiquid || isNextAir)) 
                return isNextAir;

            if (isNextLiquid)
            {
                float nextDensity = particleOnDesiredPos.Density;

                float densityDifference = currentParticle.Density - nextDensity;
                float maxDensity = currentParticle.Density + nextDensity;

                if (densityDifference > 0)
                {
                    return ThreadSafeRandom.Value < Mathf.Clamp01(densityDifference / maxDensity);
                }
            }

            return false;
        }
        
        public static bool CanOccupySpaceAirOnly(int x, int y, int nextX, int nextY, ref ParticleBoard.ParticleBoard board)
        {
            Particle.Particle particleOnDesiredPos = board[nextX, nextY];
            Particle.Particle currentParticle = board[x, y];

            return particleOnDesiredPos.Type != currentParticle.Type && particleOnDesiredPos.IsAir();
        }
    }
}