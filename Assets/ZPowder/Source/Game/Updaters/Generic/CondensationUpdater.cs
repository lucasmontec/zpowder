using System;
using UnityEngine;
using ZPowder.Particle;
using ZPowder.Updaters.Base;

namespace ZPowder.Updaters.Generic
{
    [CreateAssetMenu(menuName = Constants.Constants.ZPowderMenuBase + "Generic Updaters/Condensation Updater")]
    public class CondensationUpdater : AbstractGenericParticleUpdater
    {
        public int minLifetimeToConsiderCold = 4;
        public int minColdParticlesAroundToCondensate = 4;

        //This is here to prevent CountNeighborhood wrapping the method in a func every call, reduces GC
        private static Func<Particle.Particle, bool> _fixedColdSteamDelegate;
        private Func<Particle.Particle, bool> ColdSteamDelegate
        {
            get { return _fixedColdSteamDelegate ??= IsColdSteam; }
        }

        public override void UpdateParticle(long tick, float dt, int x, int y, ref ParticleBoard.ParticleBoard board)
        {
            Particle.Particle particle = board[x, y];

            if (particle.Type != ParticleType.Steam) return;
            
            if(board.CountNeighborhood(x, y, ColdSteamDelegate) == minColdParticlesAroundToCondensate)
            {
                board.SetParticle(x, y, tick, ParticleFactory.Water);
            }
        }

        private bool IsColdSteam(Particle.Particle p)
        {
            return p.Type == ParticleType.Steam && p.LifeTime <= minLifetimeToConsiderCold;
        }
    }
}