using UnityEngine;
using ZPowder.Particle;
using ZPowder.ParticleBoard;
using ZPowder.Updaters.Base;
using Random = Unity.Mathematics.Random;

namespace ZPowder.Updaters.Generic
{
    [CreateAssetMenu(menuName = Constants.Constants.ZPowderMenuBase + "Generic Updaters/Conduction Updater")]
    public class EnergyConductionUpdater : AbstractGenericParticleUpdater
    {
        private Random random = new(RandomSeed.Seed);

        public bool canOccupyLastSpace;
        public bool preventSpread;
        
        public override void UpdateParticle(long tick, float dt, int x, int y, ref ParticleBoard.ParticleBoard board)
        {
            ref Particle.Particle particle = ref board[x, y];

            if (particle.Type == ParticleType.Energy)
            {
                board.CheckAndChangeNeighborhood(x, y, tick, ChargeParticles);
                return;
            }

            if (!particle.IsCharged()) return;
            
            if(random.NextFloat() < 0.0002f) return;

            HandleChargedParticle(tick, x, y, board, particle);
        }

        private void HandleChargedParticle(long tick, int x, int y, ParticleBoard.ParticleBoard board, Particle.Particle particle)
        {
            int updateSideX = random.NextFloat() > 0.5f ? 1 : -1;
            int updateSideY = random.NextFloat() > 0.5f ? 1 : -1;
            bool charged = false;
            
            for (int tY = -1; tY <= 1; tY++)
            {
                for (int tX = -1; tX <= 1; tX++)
                {
                    if (tX == 0 && tY == 0) continue;

                    int realX = x + updateSideX * tX;
                    int realY = y + updateSideY * tY;

                    if (realX > board.Width - 1 || realX < 0) continue;
                    if (realY > board.Height - 1 || realY < 0) continue;

                    int lastTickDelta = 1;
                    if (canOccupyLastSpace)
                    {
                        lastTickDelta = 0;
                    }

                    if (board[realX, realY].IsFuel())
                    {
                        board[realX, realY].LifeTime = board[realX, realY].BurnTime();
                        board[realX, realY].HasLifetime = true;
                        board[realX, realY].SetBurning();
                    }

                    if (board[realX, realY].IsConductive() && !board[realX, realY].IsCharged() &&
                        board[realX, realY].LastUpdateTick < tick - lastTickDelta)
                    {
                        board[realX, realY].SetCharged();

                        if (preventSpread)
                        {
                            charged = true;
                            break;
                        }
                    }
                }

                if (charged)
                {
                    break;
                }
            }

            particle.RemoveCharged();
        }

        private static NeighborhoodParticleOperation ChargeParticles(Particle.Particle neighbor, long tick, int neighborX, int neighborY, ParticleBoard.ParticleBoard board)
        {
            if (!neighbor.IsConductive()) return NeighborhoodParticleOperation.None();
            
            neighbor.SetCharged();
            return NeighborhoodParticleOperation.SubstituteNeighbor(neighbor);
        }
    }
}