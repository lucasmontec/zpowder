# Generic Updaters

Generic updaters are updaters that work for any particle type. They usually represent updates of particle states and not
particles themselves. Generic updaters are used for ignition of flammables or condensation for example. 

They are expensive because they are called on every particle.