using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;
using ZPowder.Particle;
using ZPowder.ParticleBoard;
using ZPowder.Reactions;
using ZPowder.Threading;
using ZPowder.Updaters.Base;

namespace ZPowder.Updaters.Generic
{
    [CreateAssetMenu(menuName = Constants.Constants.ZPowderMenuBase + "Generic Updaters/Igniter Updater")]
    public class IgniterUpdater : AbstractGenericParticleUpdater, IReactionGridListener
    {
        public int tickDelayBetweenIgniteTries = 1;

        public bool hasFireIgniteChance;
        [ShowIf(nameof(hasFireIgniteChance))]
        public float fireIgniteChance = 1f;

        private ReactionGrid reactionGrid;
        
        private static readonly ParticleBoard.ParticleBoard.NeighborhoodCheckAndChangeMethod FireBurnDelegate = FireBurnOperation;
        
        public void SetReactionGrid(ReactionGrid grid)
        {
            reactionGrid = grid;
        }
        
        public override void UpdateParticle(long tick, float dt, int x, int y, ref ParticleBoard.ParticleBoard board)
        {
            Particle.Particle particle = board[x, y];

            if (!particle.IsIgniter()) return;

            bool chancePass = !hasFireIgniteChance || ThreadSafeRandom.Value < fireIgniteChance;
            if (tick % tickDelayBetweenIgniteTries == 0 && chancePass && reactionGrid.HasFuelNear(x, y))
            {
                board.CheckAndChangeNeighborhood(x, y, tick, FireBurnDelegate);
            }
        }
        
        private static NeighborhoodParticleOperation FireBurnOperation(Particle.Particle neighbor, long tick, int neighborX, int neighborY, ParticleBoard.ParticleBoard board)
        {
            //Put itself out
            if (neighbor.Type == ParticleType.Water)
            {
                return NeighborhoodParticleOperation.SubstituteSelfAndNeighbor(ParticleFactory.Air, ParticleFactory.Steam());
            }

            //Burn stuff
            if (!neighbor.IsBurning() && neighbor.IsFuel())
            {
                neighbor.LifeTime = neighbor.BurnTime();
                neighbor.TotalLifeTime = neighbor.BurnTime();
                neighbor.HasLifetime = true;
                neighbor.SetBurning();
                return NeighborhoodParticleOperation.SubstituteNeighbor(neighbor);
            }

            return NeighborhoodParticleOperation.None();
        }
    }
}