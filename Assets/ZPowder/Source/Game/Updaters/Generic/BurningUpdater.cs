using UnityEngine;
using ZPowder.Particle;
using ZPowder.Threading;
using ZPowder.Updaters.Base;

namespace ZPowder.Updaters.Generic
{
    [CreateAssetMenu(menuName = Constants.Constants.ZPowderMenuBase + "Generic Updaters/Burning Updater")]
    public class BurningUpdater : AbstractGenericParticleUpdater
    {
        public float randomFireConsumePercent = 0.3f;

        public int ticksToTryToSpawnFire = 2;
        public float fireSpawnChance = 0.1f;
        
        public override void UpdateParticle(long tick, float dt, int x, int y, ref ParticleBoard.ParticleBoard board)
        {
            Particle.Particle particle = board[x, y];

            if (!particle.IsBurning()) return;
            
            if(board.CheckNeighborhood(x, y, neighbor => neighbor.Type == ParticleType.Water))
            {
                particle.State &= ~ParticleState.Burning;
                board.SetParticle(x, y, tick, particle);
            }

            if (particle.LifeTime <= 1 || (particle.LifeTime > 1 && particle.LifeTime <= 4 && ThreadSafeRandom.Value < randomFireConsumePercent))
            {
                board.SetParticle(x, y, tick, ParticleFactory.Fire());
                return;
            }
            
            bool randomSpawnRate = particle.LifeTime % ticksToTryToSpawnFire == 0 && ThreadSafeRandom.Value < fireSpawnChance;
            bool shouldSpawnFire = randomSpawnRate && y + 1 < board.Height;
            if (shouldSpawnFire)
            {
                board.SetParticle(x, y + 1, tick, ParticleFactory.Fire());
            }
        }
    }
}