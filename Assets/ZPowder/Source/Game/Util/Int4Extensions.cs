using Unity.Mathematics;

namespace ZPowder.Util
{
    public static class Int4Extensions
    {
        public static int4 Set(ref this int4 value, int x, int y, int z, int w)
        {
            value.x = x;
            value.y = y;
            value.z = z;
            value.w = w;
            return value;
        }
    }
}