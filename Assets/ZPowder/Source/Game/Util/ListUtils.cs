using System;
using System.Collections.Generic;
using UnityEngine;

namespace ZPowder.Util
{
    public static class ListUtils {
        private static readonly System.Random Random = new(Guid.NewGuid().GetHashCode());
        
        /// <summary>
        /// Fisher Yates shuffle
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public static List<T> Shuffle<T>(this List<T> list) {
            int n = list.Count;
            for (int i = 0; i < n; i++) {
                int r = i + (int)(Random.NextDouble() * (n - i));
                T obj = list[r];
                list[r] = list[i];
                list[i] = obj;
            }

            return list;
        }
    
        public static List<T> Shuffle<T>(this List<T> list, int times) {
            for(int i=0; i<times; i++){
                list.Shuffle();
            }

            return list;
        }

        public static void Print<T>(this List<T> source) {
            string print = "[";
            source.ForEach(e => print += e+",");
            print += "]";
            Debug.Log(print);
        }
    }
}