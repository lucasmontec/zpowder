using UnityEngine;

namespace ZPowder.Util
{
    public static class IntMath
    {
        public static int Distance(int sx, int sy, int ex, int ey)
        {
            return Mathf.RoundToInt(Mathf.Sqrt((ex-sx)*(ex-sx) + (ey-sy)*(ey-sy)));
        }
    }
}