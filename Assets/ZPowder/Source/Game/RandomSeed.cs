using System;

namespace ZPowder
{
    public static class RandomSeed
    {
        public static readonly uint Seed = (uint)Guid.NewGuid().GetHashCode();
    }
}