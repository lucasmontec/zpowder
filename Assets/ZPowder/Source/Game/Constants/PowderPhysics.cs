using UnityEngine;

namespace ZPowder.Constants
{
    public static class PowderPhysics
    {
        public static Vector2 Gravity = new(0, -981);
        public const int FinalVelocity = 1100;
    }
}