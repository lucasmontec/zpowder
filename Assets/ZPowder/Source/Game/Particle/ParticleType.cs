namespace ZPowder.Particle
{
    public enum ParticleType : byte
    {
        Air = 0,
        Sand = 1,
        Water = 2,
        Rock = 3,
        Oil = 4,
        Fire = 5,
        Gasoline = 6,
        GunPowder = 7,
        Steam = 8,
        Laser = 9,
        Energy = 10,
        Copper = 11,
        Automata = 12
    }
}