using System;
using Unity.Mathematics;
using UnityEngine;
using Random = Unity.Mathematics.Random;

namespace ZPowder.Particle
{
    public static class ParticleFactory
    {
        private static Random _random = new(RandomSeed.Seed);

        public static Particle Laser(Vector2 vel)
        {
            int lifeTime = _random.NextInt(50, 80);
            return new Particle
            {
                Type = ParticleType.Laser,
                Velocity = vel,
                Density = 0f,
                HasLifetime = true,
                LifeTime = lifeTime
            };
        }

        public static Particle Fire()
        {
            int lifeTime = _random.NextInt(10, 110);
            return new Particle
            {
                Type = ParticleType.Fire,
                Velocity = Vector2.zero,
                Density = 0.01f,
                HasLifetime = true,
                TotalLifeTime = lifeTime,
                LifeTime = lifeTime
            };
        }

        public static Particle Steam()
        {
            int lifeTime = _random.NextInt(60, 220);
            return new Particle
            {
                Type = ParticleType.Steam,
                Velocity = Vector2.zero,
                Density = 0.2f,
                HasLifetime = true,
                LifeTime = lifeTime,
                TotalLifeTime = lifeTime
            };
        }
        
        public static Particle Energy()
        {
            float2 randomDir = (_random.NextFloat2Direction() + new float2(0.38f, 0.4f)) * 80;
            int lifeTime = _random.NextInt(20, 100);
            return new Particle
            {
                Type = ParticleType.Energy,
                Velocity = randomDir,
                HasLifetime = true,
                LifeTime = lifeTime,
                TotalLifeTime = lifeTime
            };
        }

        public static Particle Air = new()
        {
            Type = ParticleType.Air,
            Velocity = Vector2.zero,
            Density = 0
        };

        public static Particle Sand = new()
        {
            Type = ParticleType.Sand,
            Velocity = Vector2.zero,
            Density = 2.1f
        };

        public static Particle Water = new()
        {
            Type = ParticleType.Water,
            Velocity = Vector2.zero,
            Density = 1
        };
        
        public static Particle Rock = new()
        {
            Type = ParticleType.Rock,
            Velocity = Vector2.zero,
            Density = 100
        };
        
        public static Particle Oil = new()
        {
            Type = ParticleType.Oil,
            Velocity = Vector2.zero,
            Density = 0.853f
        };
        
        public static Particle Gasoline = new()
        {
            Type = ParticleType.Gasoline,
            Velocity = Vector2.zero,
            Density = 0.715f
        };
        
        public static Particle GunPowder = new()
        {
            Type = ParticleType.GunPowder,
            Velocity = Vector2.zero,
            Density = 2.5f
        };
        
        public static Particle Copper = new()
        {
            Type = ParticleType.Copper,
            Velocity = Vector2.zero,
            Density = 100f
        };

        public static Particle Automata = new()
        {
            Type = ParticleType.Automata,
            Velocity = Vector2.zero,
            Density = 0,
            HasLifetime = true,
            LifeTime = 150,
            TotalLifeTime = 150
        };
        
        public static Particle FromType(ParticleType type)
        {
            return type switch
            {
                ParticleType.Air => Air,
                ParticleType.Sand => Sand,
                ParticleType.Water => Water,
                ParticleType.Rock => Rock,
                ParticleType.Oil => Oil,
                ParticleType.Fire => Fire(),
                ParticleType.Gasoline => Gasoline,
                ParticleType.GunPowder => GunPowder,
                ParticleType.Steam => Steam(),
                ParticleType.Laser => Laser(UnityEngine.Random.insideUnitCircle),
                ParticleType.Energy => Energy(),
                ParticleType.Copper => Copper,
                ParticleType.Automata => Automata,
                _ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
            };
        }
    }
}