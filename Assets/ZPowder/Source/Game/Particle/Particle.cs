using UnityEngine;

namespace ZPowder.Particle
{
    public struct Particle
    {
        public ParticleType Type;
        public ParticleState State;
        
        public Vector2 Velocity;
        public float Density;
        public long LastUpdateTick;

        public bool HasLifetime;
        public int LifeTime;
        public int TotalLifeTime;

        public float NormalizedLifetime => 1f - (LifeTime * 1f / TotalLifeTime);
        
        public bool IsUpdated(long tick)
        {
            return LastUpdateTick == tick;
        }
    }
}