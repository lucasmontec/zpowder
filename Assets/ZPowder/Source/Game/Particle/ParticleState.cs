using System;

namespace ZPowder.Particle
{
    [Flags]
    public enum ParticleState : byte
    {
        None = 0,
        Burning = 1 << 1,
        Damaged = 1 << 2, //Uses lifetime to represent damage, has lifetime = false,
        Charged = 1 << 3
    }
}