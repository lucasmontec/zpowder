namespace ZPowder.Particle
{
    public static class ParticleExtensions
    {
        public static bool IsBurning(this Particle particle)
        {
            return (particle.State & ParticleState.Burning) != 0;
        }
        
        public static void SetBurning(this ref Particle particle)
        {
            particle.State |= ParticleState.Burning;
        }
        
        public static bool IsDamaged(this Particle particle)
        {
            return (particle.State & ParticleState.Damaged) != 0;
        }

        public static void SetDamaged(this ref Particle particle)
        {
            particle.State |= ParticleState.Damaged;
        }

        public static bool IsAir(this Particle particle)
        {
            return particle.Type == ParticleType.Air;
        }
        
        public static bool IsLiquid(this Particle particle)
        {
            return particle.Type == ParticleType.Water || 
                   particle.Type == ParticleType.Oil ||
                   particle.Type == ParticleType.Gasoline;
        }
        
        public static bool IsGas(this Particle particle)
        {
            return particle.Type == ParticleType.Steam;
        }
        
        public static bool IsConductive(this Particle particle)
        {
            return particle.Type == ParticleType.Copper ||
                   particle.Type == ParticleType.Water;
        }
        
        public static bool IsCharged(this Particle particle)
        {
            return (particle.State & ParticleState.Charged) != 0;
        }
        
        public static void RemoveCharged(this ref Particle particle)
        {
            particle.State &= ~ParticleState.Charged;
        }
        
        public static void SetCharged(this ref Particle particle)
        {
            particle.State |= ParticleState.Charged;
        }

        public static bool IsIgniter(this Particle particle)
        {
            return particle.Type == ParticleType.Fire ||
                   particle.Type == ParticleType.Energy;
        }
        
        public static bool IsFuel(this Particle particle)
        {
            return particle.Type == ParticleType.Oil ||
                   particle.Type == ParticleType.Gasoline ||
                   particle.Type == ParticleType.GunPowder;
        }

        public static int Hardness(this Particle particle)
        {
            return particle.Type switch
            {
                ParticleType.Water => 0,
                ParticleType.Oil => 5,
                ParticleType.Gasoline => 2,
                ParticleType.Rock => 100,
                ParticleType.Sand => 20,
                ParticleType.GunPowder => 15,
                _ => 0
            };
        }
        
        //0 - 100
        public static int Viscosity(this Particle particle)
        {
            return particle.Type switch
            {
                ParticleType.Water => 25,
                ParticleType.Oil => 70,
                ParticleType.Gasoline => 15,
                _ => 0
            };
        }
        
        public static int Volatility(this Particle particle)
        {
            return particle.Type switch
            {
                ParticleType.Steam => 60,
                _ => 0
            };
        }
        
        public static int BurnTime(this Particle particle)
        {
            if (!IsFuel(particle))
            {
                return 0;
            }

            return particle.Type switch
            {
                ParticleType.Oil => 20,
                ParticleType.Gasoline => 4,
                ParticleType.GunPowder => 2,
                _ => 10
            };
        }
    }
}