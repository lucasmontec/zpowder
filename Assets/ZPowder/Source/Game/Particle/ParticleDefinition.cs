using System;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using ZPowder.Updaters;
using ZPowder.Updaters.Base;

namespace ZPowder.Particle
{
    [CreateAssetMenu(menuName = Constants.Constants.ZPowderMenuBase + "Particle Definition")]
    public class ParticleDefinition : SerializedScriptableObject
    {
        /*
        public Color primaryColor;
        public Color secondaryColor;
        */

        [OdinSerialize, NonSerialized, AssetSelector]
        public AbstractParticleUpdater ParticleUpdater;
    }
}